﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinsManager : MonoBehaviour
{
   public static SkinsManager Current;
   public GameObject footballPrefab;
   public GameObject defaultSphere;
   public GameObject playerPrefab;
   public static SkinAsset footballSkin;
   public static SkinAsset defaultSphereSkin;
   private int _currentSkin = 0;
   public struct SkinAsset
   {
      public Mesh mesh;
      public Material material;
      public Material[] materials;
      public MeshFilter meshFilter;
      public MeshRenderer meshRenderer;

      public SkinAsset(GameObject gameObject)
      {
         meshRenderer = gameObject.GetComponent<MeshRenderer>();
         meshFilter = gameObject.GetComponent<MeshFilter>();
         mesh = meshFilter.mesh;
         material = meshRenderer.material;
         materials = meshRenderer.materials;
      }
   }

   private void SetSkinData(SkinAsset skinAsset)
   {
      defaultSphere.GetComponent<MeshFilter>().mesh = skinAsset.mesh;
      defaultSphere.GetComponent<MeshRenderer>().materials = skinAsset.materials;
      defaultSphere.GetComponent<MeshRenderer>().material = skinAsset.material;
   }

   public void ChangeSkin()
   {
      if(ShopController.Current.currentSkin == 0)
      {
         SetSkinData(defaultSphereSkin);
         _currentSkin = 0;
      } else
      if(ShopController.Current.currentSkin == 1)
      {
         SetSkinData(footballSkin);
         _currentSkin = 1;
      }
   }
   private void Start()
   {
      footballSkin = new SkinAsset(footballPrefab);
      defaultSphereSkin = new SkinAsset(defaultSphere);
      if(_currentSkin != ShopController.Current.currentSkin) ChangeSkin();
   }

   private void Awake()
   {
      Current = this;
   }
}
