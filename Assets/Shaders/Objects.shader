﻿Shader "Feelnside/Objects"
{
    Properties
    {
        _Color ("Color", Color) = (0.5, 0.5, 0.5, 1)
        _Treshold ("Cel treshold", Range(1., 20.)) = 5.

        [Header(Texture Availability)]
        _MainTex ("Texture", 2D) = "white" {}
        
        [Header(Rim Settings)]
        [Toggle] RIM("Rim Effect", Int) = 0
        _RimValue ("Rim Value", Range(0.0,2.0)) = 1.0
        
        [Header(Reflection Settings)]
        [Toggle] REFL("Reflection Effect", Int) = 0
        _ReflPower("Reflection Power", Float) = 0.5

        [Header(Fog Settings)]
        _FogStart ("Fog Start", Range (0, 500)) = 20
        _FogEnd ("Fog End", Range (0, 500)) = 100
        _FogUp ("Up Fog Position", Float) = 5
        _FogBottom ("Bottom Fog Position", Float) = 1
    }
    
    SubShader
    {
        Tags { "RenderType" = "Opaque" "Queue" = "Geometry+5" "IgnoreProjector" = "True"}
        Cull Back
        ZWrite On
        ZTest LEqual
        Blend Off

        Pass
        {
            Tags { "LightMode" = "ForwardBase" "PassFlags" = "OnlyDirectional" }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile __ RIM_ON
            #pragma multi_compile __ REFL_ON
            #pragma multi_compile_fwdbase
            #pragma skip_variants LIGHTPROBE_SH LIGHTMAP_SHADOW_MIXING DYNAMICLIGHTMAP_ON DIRLIGHTMAP_COMBINED LIGHTMAP_ON SHADOWS_CUBE SHADOWS_DEPTH VERTEXLIGHT_ON
            #pragma fragmentoption ARB_precision_hint_fastest
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "AutoLight.cginc"
            
            half4 _Color;
            half _Treshold;
            
            half4 _worldPos;
            half3 _worldNormal;

            half3 _worldViewDir;
            
            sampler2D _MainTex;
            half4 _MainTex_ST;

            //fog
            half _FogStart;
            half _FogEnd;
            half _FogUp;
            half _FogBottom;
        
            #if RIM_ON
            half _RimValue;
            #endif

            #if REFL_ON
            uniform half _ReflPower;
            samplerCUBE _ReflCube;
            #endif
            
            struct appdata
            {
                float4 vertex : POSITION;
                half3 normal : NORMAL;
                fixed3 color : COLOR;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                SHADOW_COORDS(0) // put shadows data into TEXCOORD0
                fixed3 diff : COLOR0;
                fixed3 ambient : COLOR1;
                fixed3 color : COLOR2;
                float4 pos : SV_POSITION;
                
                #if RIM_ON
                half colorRefl : COLOR4;
                #endif

                #if REFL_ON
                half3 worldRefl : TEXCOORD2;
                #endif

                half fog : COLOR6;

                half3 shadow : COLOR5;
                
                float2 uv : TEXCOORD3;
            };

            float LightToonShading(float3 normal, float3 lightDir)
            {
                float NdotL = max(0, dot(normalize(normal), lightDir));
                return floor(NdotL * _Treshold) / (_Treshold - 0.5);
            }
            
            v2f vert (appdata v)
            {
                v2f o;

                _worldPos.xyz = mul(unity_ObjectToWorld, v.vertex).xyz;
                _worldNormal = UnityObjectToWorldNormal(v.normal);

                _worldPos.w = 1;

                o.pos = mul(UNITY_MATRIX_VP, _worldPos);

                o.color = _Color * v.color;
                o.diff = max(0, dot(_worldNormal, _WorldSpaceLightPos0.xyz)) * _LightColor0.rgb;
                o.ambient.rgb = ShadeSH9(half4(_worldNormal,1));

                #if RIM_ON || REFL_ON
                _worldViewDir = normalize(UNITY_MATRIX_I_V._m03_m13_m23 - _worldPos.xyz).xyz;
                #endif
                
                #if RIM_ON
                o.colorRefl = _RimValue - saturate(dot(_worldViewDir, _worldNormal.xyz));
                o.colorRefl *= o.colorRefl;
                o.colorRefl *= o.colorRefl;
                #endif

                #if REFL_ON
                o.worldRefl = reflect(-_worldViewDir, _worldNormal);
                #endif
                
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);

                // compute shadows data
                TRANSFER_SHADOW(o)

                //fog            
                o.fog = max(saturate((dot(UNITY_MATRIX_V._m20_m21_m22_m23, _worldPos) + _FogStart) / (_FogStart - _FogEnd)), saturate((_worldPos.y - _FogUp) / (_FogBottom - _FogUp)));

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = half4(i.color * tex2D(_MainTex, i.uv), 1);
                col.rgb *= i.diff * SHADOW_ATTENUATION(i) + i.ambient.rgb;
                
                #if RIM_ON
                col.rgb += i.colorRefl;
                #endif

                #if REFL_ON
                half3 refl = texCUBE(_ReflCube, i.worldRefl);
                refl *= refl;
                refl *= refl;
                col.rgb += refl * _ReflPower;
                #endif

                //fog
                col.rgb = lerp(col.rgb, unity_FogColor, i.fog);
                
                return col;
            }
            ENDCG
        }
        
        // shadow caster rendering pass, implemented manually
        // using macros from UnityCG.cginc
        Pass
        {
            Tags {"LightMode"="ShadowCaster"}

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_shadowcaster
            #include "UnityCG.cginc"
            
            struct appdata
            {
                float4 vertex : POSITION;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            { 
                V2F_SHADOW_CASTER;
            };

            v2f vert(appdata v)
            {
                v2f o;
                
                TRANSFER_SHADOW_CASTER(o)

                return o;
            }

            float4 frag(v2f i) : SV_Target
            {
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
}