﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShopController : MonoBehaviour
{
    public int currentSkin = 0;
    public int isFootballSold = 0;
    public int isBasketballSold = 0;
    public int isKolobokSold = 0;
    public GameObject footballBuyButton;
    public GameObject basketballBuyButton;
    public GameObject kolobokBuyButton;
    public GameObject footbalChoose;
    public GameObject basketballChoose;
    public GameObject defaultSkinChoose;
    public GameObject kolobokChoose;
    public static ShopController Current;

    private void Awake()
    {
        Current = this;
    }

    private void Start()
    {
        CheckForPurchasesButtons();
    }

    public void CheckForPurchasesButtons()
    {
        if (isFootballSold == 1) footballBuyButton.GetComponent<Button>().interactable = false;
        if (isBasketballSold == 1) basketballBuyButton.GetComponent<Button>().interactable = false;
        if (isKolobokSold == 1) kolobokBuyButton.GetComponent<Button>().interactable = false;
        if(currentSkin == 0)  defaultSkinChoose.GetComponent<Image>().color = Color.red;
        if(currentSkin == 1)  footbalChoose.GetComponent<Image>().color = Color.red;
        if(currentSkin == 2)  basketballChoose.GetComponent<Image>().color = Color.red;
        if(currentSkin == 3)  kolobokChoose.GetComponent<Image>().color = Color.red;
    }
    public void BuyFootball()
    {
        var price = 100000;
        if (Settings.Current.starsCount >= price && isFootballSold == 0)
        {
            Settings.Current.starsCount -= price;
            isFootballSold = 1;
            Settings.Current.SaveGameSettings();
        }
        CheckForPurchasesButtons();
    }
    public void BuyBasketball()
    {
        var price = 100000;
        if (Settings.Current.starsCount >= price && isBasketballSold == 0)
        {
            Settings.Current.starsCount -= price;
            isBasketballSold = 1;
            Settings.Current.SaveGameSettings();
        }
        CheckForPurchasesButtons();
    }
    public void BuyKolobok()
    {
        var price = 100000;
        if (Settings.Current.starsCount >= price && isKolobokSold == 0)
        {
            Settings.Current.starsCount -= price;
            isKolobokSold = 1;
            Settings.Current.SaveGameSettings();
        }
        CheckForPurchasesButtons();
    }

    public void ChooseDefaultSkin()
    {
        currentSkin = 0;
        defaultSkinChoose.GetComponent<Image>().color = Color.red;
        footbalChoose.GetComponent<Image>().color = Color.white;
        kolobokChoose.GetComponent<Image>().color = Color.white;
        basketballChoose.GetComponent<Image>().color = Color.white;
        Settings.Current.SaveCurrentSkin();
    }

    public void ChooseFootball()
    {
        if(isFootballSold == 1)
        {
            currentSkin = 1;
            footbalChoose.GetComponent<Image>().color = Color.red;
            kolobokChoose.GetComponent<Image>().color = Color.white;
            basketballChoose.GetComponent<Image>().color = Color.white;
            defaultSkinChoose.GetComponent<Image>().color = Color.white;
        }
        Settings.Current.SaveCurrentSkin();
    }
    public void ChooseBasketball()
    {
        if(isBasketballSold == 1)
        {
            currentSkin = 2;
            basketballChoose.GetComponent<Image>().color = Color.red;
            kolobokChoose.GetComponent<Image>().color = Color.white;
            footbalChoose.GetComponent<Image>().color = Color.white;
            defaultSkinChoose.GetComponent<Image>().color = Color.white;
        }
        Settings.Current.SaveCurrentSkin();

    }
    public void ChooseKolobok()
    {
        if(isKolobokSold == 1)
        {
            currentSkin = 3;
            kolobokChoose.GetComponent<Image>().color = Color.red;
            footbalChoose.GetComponent<Image>().color = Color.white;
            basketballChoose.GetComponent<Image>().color = Color.white;
            defaultSkinChoose.GetComponent<Image>().color = Color.white;
        }
        Settings.Current.SaveCurrentSkin();
    }

    public void ResetProgress()
    {
        PlayerPrefs.DeleteKey("isFootballSold");
        PlayerPrefs.DeleteKey("isBasketballSold");
        PlayerPrefs.DeleteKey("isKolobokSold");
    }
}
