﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarCollect : MonoBehaviour
{
    [SerializeField] private ParticleSystem ps1;
    [SerializeField] private ParticleSystem ps2;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Settings.Current.starsCount += 100;
            ps1.Play();
            ps2.Play();
            Destroy(gameObject);
        }
    }
}
