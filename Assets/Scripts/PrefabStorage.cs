﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabStorage : MonoBehaviour
{
   static public PrefabStorage current;
   public List<GameObject> prefabsSegments = new List<GameObject>();
   public List<GameObject> prefabsColorWays = new List<GameObject>();
   private void Awake()
   {
      current = this;
   }
}
