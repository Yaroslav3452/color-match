﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CanvasesManager : MonoBehaviour
{
    static public CanvasesManager Current;
    public TextMeshProUGUI starsCount;
    public TextMeshProUGUI distance;
    public TextMeshProUGUI qualityText;
    public TextMeshProUGUI volumeText;
    public TextMeshProUGUI vibrationText;
    public List<GameObject> Canvases = new List<GameObject>();
    private void Awake()
    {
        Current = this;
    }

    public void OpenSettings()
    {
        foreach (var canvas in Canvases)
        {
            canvas.SetActive(false);
        }
        Canvases[3].SetActive(true);
        SetSliderVolume();
    }

    public void CloseSettings()
    {
        Canvases[1].SetActive(true);
        Canvases[3].SetActive(false);
        Canvases[4].SetActive(true);
        Settings.Current.SaveGameSettings();
    }

    public void MainMenuOpen()
    {
        Canvases[0].SetActive(false);
        Canvases[1].SetActive(true);
        Canvases[2].SetActive(false);
        Canvases[3].SetActive(false);
        Canvases[4].SetActive(false);
        Canvases[5].SetActive(false);
        distance.gameObject.SetActive(true);
    }
    public void SetQualityText()
    {
        if(Settings.Current.qualitySettings == 0)
        {
            qualityText.text = "Current - Low";
        }
        else
        {
            qualityText.text = "Current - High";
        }
    }
    public void VolumeSliderTextChanger()
    {
        Settings.Current.volume = (int)volumeText.GetComponentInParent<Slider>().value;
        volumeText.text = Settings.Current.volume.ToString();
        if(AudioManager.Current) AudioManager.Current.SetMasterVolume();
    }

    public void SetSliderVolume()
    {
        volumeText.GetComponentInParent<Slider>().value = Settings.Current.volume;
    }
    public void SetVibrationText()
    {
        if (Settings.Current.vibration == 1) vibrationText.text = "On";
        else vibrationText.text = "off";
    }

    public void PrivacyPolicy()
    {
        Application.OpenURL("https://docs.google.com/document/d/1pEnfCv_90MjmWKeh3u7Rgt2R1A6PaV-x3n4wEOF8VNg/edit?usp=sharing");
    }

    public void CloseShop()
    {
        Canvases[1].SetActive(true);
        Canvases[0].SetActive(false);
        Canvases[2].SetActive(false);
        Canvases[3].SetActive(false);
        Canvases[4].SetActive(false);
        Canvases[5].SetActive(false);
        distance.gameObject.SetActive(true);
    }

    public void OpenShop()
    {
        Canvases[1].SetActive(false);
        Canvases[5].SetActive(true);
        Canvases[0].SetActive(false);
        Canvases[2].SetActive(false);
        Canvases[3].SetActive(false);
        Canvases[4].SetActive(false);
        distance.gameObject.SetActive(false);
    }
}
