﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviour
{
   static public ParticleManager Current;
   [SerializeField] private List<GameObject> ExplosionPrefabs = new List<GameObject>();
   private ParticleSystem.MainModule _prtModuleMagicPowder;
   private ParticleSystem.MainModule _prtModuleShockWave;


   public void Awake()
   {
      Current = this;
      _prtModuleMagicPowder = PlayerMovement.Current.playerParticleModule[0].main; 
      _prtModuleShockWave = PlayerMovement.Current.playerParticleModule[1].main;
   }

   public void BoomEffect(Transform localTransform)
   {
      var boomGameObject = Instantiate(ExplosionPrefabs[0], localTransform.position, Quaternion.identity);
      Destroy(boomGameObject, 2);
   }

   public void ChangeColorEffect()
   {
      if (PlayerMovement.Current.playerColor == "red")
      {
         _prtModuleMagicPowder.startColor = Color.red;
         _prtModuleShockWave.startColor = Color.red;
      }
      else if(PlayerMovement.Current.playerColor == "yellow")
      {
         _prtModuleMagicPowder.startColor = Color.yellow;
         _prtModuleShockWave.startColor = Color.yellow;
      }
      else if(PlayerMovement.Current.playerColor == "green")
      {
         _prtModuleMagicPowder.startColor = Color.green;
         _prtModuleShockWave.startColor = Color.green;
      }
      PlayerMovement.Current.playerParticleModule[0].Play();
      PlayerMovement.Current.playerParticleModule[1].Play();

   }
}
