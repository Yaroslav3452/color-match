﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager : MonoBehaviour
{
    public GameObject startSurface;
    static public SceneManager Current;
    private bool _isFirstGame = true;
    private void Awake()
    {
        Current = this;
    }

    public void RestartLevel()
    {
        GameObject[] arr = GameObject.FindGameObjectsWithTag("SurfaceChunk");
        foreach (var chunk in arr)
        {
            Destroy(chunk);
        }
        startSurface.GetComponentInChildren<ChunkGenerator>().isCreated = false;
        CanvasesManager.Current.Canvases[0].SetActive(false);
        CanvasesManager.Current.Canvases[2].SetActive(false);
        PlayerMovement.Current.transform.position = new Vector3(0,0.6f,0);
        PlayerMovement.Current.trailRenderer.enabled = true;
        PlayerMovement.Current._transform.localScale = new Vector3(1, 1, 1);
        PlayerMovement.Current.playerRb.isKinematic = false;
        ResetValues();
    }

    public void StartGame()
    {
       
        if(_isFirstGame == false) RestartLevel();
        else
        {
            _isFirstGame = false;
            ResetValues();
        }
        PlayerMovement.Current.playerRb.isKinematic = false;
        CanvasesManager.Current.Canvases[1].SetActive(false);
        CanvasesManager.Current.Canvases[4].gameObject.SetActive(true);
        CanvasesManager.Current.distance.gameObject.SetActive(true);
        CanvasesManager.Current.starsCount.gameObject.SetActive(false);
    }

    public void ResetValues()
    {
        PlayerMovement.Current.distance = 0;
        PlayerMovement.Current.newcolor = PlayerMovement.Current.meshRenderer.material.color;
        PlayerMovement.Current.isEnd = false;
        PlayerMovement.Current.audioSource.Play();
        PlayerMovement.Current.newSize = Vector3.one;
        PlayerMovement.Current.frwdSpeed = 10;
        PlayerMovement.Current.playerRadius = 1;
    }
}
