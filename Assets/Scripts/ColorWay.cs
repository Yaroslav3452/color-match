﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorWay : MonoBehaviour
{
   public string colorOfColorWay;
   private void OnTriggerEnter(Collider other)
   {
       if (other.CompareTag("Player")) PlayerMovement.Current.PlayerChangeColor(colorOfColorWay);
   }
}
