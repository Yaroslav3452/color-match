﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public GameObject player;
    private Vector3 _offset;

    private void Start()
    {
        _offset = transform.position - player.transform.position;
    }

    private void FixedUpdate()
    {
        var targetPosition= player.transform.position + _offset;
        targetPosition.x = 0;
        transform.position = targetPosition;
    }
}
