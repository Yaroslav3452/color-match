﻿using UnityEngine;

public class Bomb : MonoBehaviour
{
    [SerializeField] LayerMask targetLayer;
    [SerializeField] GameObject bombParts;
    public void Kaboom(Transform transform, int radius)
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius, targetLayer);
        foreach (var hit in hitColliders)
        {
            var rb = hit.GetComponent<Rigidbody>();
            rb.isKinematic = false;
            rb.AddExplosionForce(20f, transform.position, 32f, 0.5f, ForceMode.VelocityChange);
        }
        ParticleManager.Current.BoomEffect(transform);
        AudioManager.Current.PlayBombSound(transform.position);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if(Settings.Current.vibration == 1) Handheld.Vibrate();
            Kaboom(transform, 10);
            Destroy(bombParts);
        }
    }
}
