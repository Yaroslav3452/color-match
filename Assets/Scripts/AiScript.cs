﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiScript : MonoBehaviour
{
    static public AiScript Current;
    public List<Transform> bombs = new List<Transform>();
    public List<Transform> pushers = new List<Transform>();
    public List<Transform> colorSurfaces = new List<Transform>();
    public List<Sphere> spheres = new List<Sphere>();
    public GameObject prefab;
    public List<Vector3> waypoints = new List<Vector3>();
    private float _colorWayLength;
    private float _colorWayWidth;
    private Transform _triggerTransform;
    private Vector3 _offset;
    private Vector3 _defaultPos;
    private Transform _transformPlayer;

    public void ClearArrays()
    {
        bombs.Clear();
        pushers.Clear();
        colorSurfaces.Clear();
        spheres.Clear();
    }

    private void Awake()
    {
        Current = this;
    }

    private void Start()
    {
        _offset = transform.position - PlayerMovement.Current._transform.position;
        _offset.x = 0;
        _offset.y = 0;
        _triggerTransform = transform;
        _transformPlayer = PlayerMovement.Current._transform;
        _defaultPos = transform.position;
        _defaultPos.z = 0;
        var lossyScale = prefab.transform.lossyScale;
        _colorWayWidth = lossyScale.x;
        _colorWayLength = lossyScale.z;
    }

    private void OnTriggerExit(Collider other)
    {

        bombs.Remove(other.transform);
        pushers.Remove(other.transform);
        spheres.Remove(other.GetComponent<Sphere>());
        colorSurfaces.Remove(other.transform);
    }
    
    /// <summary>
/// Сбор данных об объектах перед игроком
/// </summary>
/// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Sphere") && !spheres.Contains(other.gameObject.GetComponent<Sphere>()))
        {
            spheres.Add(other.gameObject.GetComponent<Sphere>());
        }
        else if (other.gameObject.CompareTag("ColorWay") && !colorSurfaces.Contains(other.gameObject.transform))
        {
            colorSurfaces.Add(other.transform);
        }
        else if (other.gameObject.CompareTag("Bomb") && !bombs.Contains(other.gameObject.transform))
        {
            bombs.Add(other.gameObject.transform);
        }
        else if (other.CompareTag("SphereOfPush") && !pushers.Contains(other.gameObject.transform))
        {
            pushers.Add(other.gameObject.transform);
        }
    }

    private void CheckForObjects()
    {
        Vector3 sphereVector3 = Vector3.zero;
        Transform secondChanceSphere = null;
        Transform secondChanceColorWay = null;
        foreach (var sphere in spheres)
        {
            if (sphere.colorOfSphere == PlayerMovement.Current.playerColor)
            {
                sphereVector3 = sphere.transform.position;
            }
            else if(sphere.transform.position.z - PlayerMovement.Current.targetPosition.z < 10f
                    && Math.Abs(sphere.transform.position.x - PlayerMovement.Current.targetPosition.x) < 0.5f)
            {
                Vector3 avoid = sphere.transform.position;
                avoid.y = 0;
                if (sphere.transform.position.x >= 0f)
                {
                    avoid.x -= 0.5f;
                } else if (sphere.transform.position.x <= 0f)
                {
                    avoid.x += 0.5f;
                }
                avoid.z -= 5;
                if(!waypoints.Contains(avoid)) waypoints.Add(avoid);
            }
        }
        if (sphereVector3 == Vector3.zero && colorSurfaces.Count > 0)
        {
            foreach (var colorSurface in colorSurfaces)
            {
                var color = colorSurface.GetComponentInChildren<ColorWay>().colorOfColorWay;
                foreach (var sphere in spheres)
                {
                    if(sphere.colorOfSphere == color && sphere.transform.position.z > colorSurface.transform.position.z - _colorWayLength)
                    {
                        secondChanceColorWay = colorSurface;
                        secondChanceSphere = sphere.transform;
                    }
                }
            }
        }
        if (sphereVector3 != Vector3.zero && Math.Abs(PlayerMovement.Current.targetPosition.x - sphereVector3.x) > 0 &&
            sphereVector3.x > -2.20f && sphereVector3.x < 2.20f)
        {
            if(!waypoints.Contains(sphereVector3))waypoints.Add(sphereVector3);
            //PlayerMovement.Current.targetAIPosition.x = sphereVector3.x;
        } else
        if (sphereVector3 == Vector3.zero && secondChanceSphere != null && secondChanceColorWay != null)
        {
            if(!waypoints.Contains(secondChanceColorWay.position))waypoints.Add(secondChanceColorWay.position);
            if(!waypoints.Contains(secondChanceSphere.position))waypoints.Add(secondChanceSphere.position);
            //PlayerMovement.Current.targetAIPosition.x = secondChanceColorWay.position.x;
        }
        if (waypoints.Count != 0)
        {
            PlayerMovement.Current.targetAIPosition.x = waypoints[0].x;
            if(PlayerMovement.Current.targetPosition.z > waypoints[0].z) waypoints.Remove(waypoints[0]);
        }
        else PlayerMovement.Current.targetAIPosition.x = _defaultPos.x;
    }
    private void FixedUpdate()
    {
        _triggerTransform.position = _offset + PlayerMovement.Current._transform.position;
        CheckForObjects();
    }
}
