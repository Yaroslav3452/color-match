﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using Random = UnityEngine.Random;

public class AudioManager : MonoBehaviour
{
    public AudioMixer audioMixer;
    static public AudioManager Current;
    [SerializeField] private GameObject audioSource2dGameObject;
    [SerializeField] private GameObject audioSource3dSpheresGameObject;
    [SerializeField] private GameObject audioSource3dBombsGameObject;
    [SerializeField] private GameObject audioSource3dPushersGameObject;
    [SerializeField] private List<AudioClip> audioClick = new List<AudioClip>();
    [SerializeField] private List<AudioClip> audioLoose = new List<AudioClip>();
    [SerializeField] private List<AudioClip> audioWin = new List<AudioClip>();
    [SerializeField] private List<AudioClip> audioSphereGrow = new List<AudioClip>();
    [SerializeField] private List<AudioClip> audioSphereDecrease = new List<AudioClip>();
    [SerializeField] private List<AudioClip> audioBomb = new List<AudioClip>();
    [SerializeField] private List<AudioClip> audioPusher = new List<AudioClip>();
    private AudioSource _audioSource2d;
    private AudioSource _audioSource3dSpheres;
    private AudioSource _audioSource3dPushers;
    private AudioSource _audioSource3dBombs;
    private Transform _audioSourse2dTransform;
    private Transform _audioSourse3dSpheresTransform;
    private Transform _audioSourse3dPushersTransform;
    private Transform _audioSourse3dBombsTransform;


    private void Awake()
    {
        Current = this;
        _audioSource2d = audioSource2dGameObject.GetComponent<AudioSource>();
        _audioSourse2dTransform = audioSource2dGameObject.transform;
        _audioSource3dSpheres = audioSource3dSpheresGameObject.GetComponent<AudioSource>();
        _audioSourse3dSpheresTransform = audioSource3dSpheresGameObject.transform;
        _audioSourse3dPushersTransform = audioSource3dPushersGameObject.transform;
        _audioSource3dPushers = audioSource3dPushersGameObject.GetComponent<AudioSource>();
        _audioSourse3dBombsTransform = audioSource3dBombsGameObject.transform;
        _audioSource3dBombs = audioSource3dBombsGameObject.GetComponent<AudioSource>();
    }

    public void PlaySoundClick()
    {
        _audioSource2d.clip = audioClick[0];
        _audioSource2d.pitch = Random.Range(0.9f, 1.1f);
        _audioSource2d.Play();
    }
    public void PlayWinSound()
    {
        int idx = Random.Range(0, audioWin.Count);
        _audioSource2d.clip = audioWin[idx];
        _audioSource2d.pitch = Random.Range(0.9f, 1.1f);
        _audioSource2d.Play();
    }
    public void PlayLooseSound()
    {
        int idx = Random.Range(0, audioLoose.Count);
        _audioSource2d.clip = audioLoose[idx];
        _audioSource2d.pitch = Random.Range(0.9f, 1.1f);
        _audioSource2d.Play();
    }
    public void PlaySphereGrowSound(Vector3 positionOfSound)
    {
        int randomIdx = Random.Range(0, audioSphereGrow.Count);
        _audioSource3dSpheres.clip = audioSphereGrow[randomIdx];
        _audioSource3dSpheres.pitch = Random.Range(0.9f, 1.1f);
        _audioSourse3dSpheresTransform.position = positionOfSound;
        _audioSource3dSpheres.Play();
    }
    public void PlaySphereDecreaseSound(Vector3 positionOfSound)
    {
        int randomIdx = Random.Range(0, audioSphereDecrease.Count);
        _audioSource3dSpheres.clip = audioSphereDecrease[randomIdx];
        _audioSource3dSpheres.pitch = Random.Range(0.9f, 1.1f);
        _audioSourse3dSpheresTransform.position = positionOfSound;
        _audioSource3dSpheres.Play();
    }
    public void PlayBombSound(Vector3 positionOfSound)
    {
        int randomIdx = Random.Range(0, audioBomb.Count);
        _audioSource3dBombs.clip = audioBomb[randomIdx];
        _audioSource3dBombs.pitch = Random.Range(0.9f, 1.1f);
        _audioSourse3dBombsTransform.position = positionOfSound;
        _audioSource3dBombs.Play();
    }
    public void PlayPusherSound(Vector3 positionOfSound)
    {
        int randomIdx = Random.Range(0, audioPusher.Count);
        _audioSource3dPushers.clip = audioPusher[randomIdx];
        _audioSource3dPushers.pitch = Random.Range(0.9f, 1.1f);
        _audioSourse3dPushersTransform.position = positionOfSound;
        _audioSource3dPushers.Play();
    }

    public void SetMasterVolume()
    {
        audioMixer.SetFloat("Master", Settings.Current.volume - 10);
    }
}
