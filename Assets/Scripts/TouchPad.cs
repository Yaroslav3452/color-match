﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

public class TouchPad : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    public Vector2 originDrag;
    public Vector2 currentPosition = Vector2.zero;
    public Vector2 directionDrag;
    public float distance;
    public Vector3 startDragPlayerPosition;
    
    public void OnPointerDown(PointerEventData eventData)
    {
        originDrag = eventData.position;
        startDragPlayerPosition = PlayerMovement.Current._transform.position;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        directionDrag = Vector2.zero;
    }

    public void OnDrag(PointerEventData eventData)
    {
        currentPosition = eventData.position;
        Vector2 directionRaw = originDrag - currentPosition;
        distance = directionRaw.magnitude / Screen.width;
        directionDrag = directionRaw.normalized;
    }
}
