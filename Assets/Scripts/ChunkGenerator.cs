﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using Random = System.Random;

public class ChunkGenerator : MonoBehaviour
{
   public GameObject endPoint;
   public bool isCreated = false;
   private void OnTriggerEnter(Collider other)
   {
      if(other.gameObject.CompareTag("Player") && !isCreated)
      {
         SpawnSegment();
         isCreated = true;
      }
   }

   private void OnTriggerExit(Collider other)
   {
      if(other.gameObject.CompareTag("Player") && !CompareTag("Respawn"))
      {
         Destroy(transform.root.gameObject, 15);
      }
   }

   private void SpawnSegment()
   {
      Random random = new Random();
      int segmentIdx = random.Next(PrefabStorage.current.prefabsSegments.Count);
      if (segmentIdx == 3 || segmentIdx == 4)
      {
         var chance = random.Next(1,5);
         if(chance != 1) segmentIdx = random.Next(PrefabStorage.current.prefabsSegments.Count);
      }
      Instantiate(PrefabStorage.current.prefabsSegments[segmentIdx], endPoint.transform.position, Quaternion.identity);
   }
}
