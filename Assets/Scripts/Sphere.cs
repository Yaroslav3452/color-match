﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sphere : MonoBehaviour
{
   public string colorOfSphere;

   private void OnTriggerEnter(Collider other)
   {
      if (other.gameObject.CompareTag("Player")){
         if(Settings.Current.vibration == 1) Handheld.Vibrate();
         var playerMovementScript = PlayerMovement.Current;
         if (playerMovementScript.playerColor == colorOfSphere)
         {
            AudioManager.Current.PlaySphereGrowSound(transform.position);
            playerMovementScript.PlayerGrow();
         }
         else
         {
            AudioManager.Current.PlaySphereDecreaseSound(transform.position);
            playerMovementScript.PlayerDecrease();
         }
         Destroy(gameObject);
      }

      if (other.gameObject.CompareTag("SphereOfPush"))
      {
         GetComponent<Rigidbody>().isKinematic = false;
      }
   }
}
