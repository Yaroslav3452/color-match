﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Audio;

public class Settings : MonoBehaviour
{
    static public Settings Current;
    public int starsCount;
    public int vibration;
    public int qualitySettings;
    public int volume;
    public int sensitivity = 2;
    public List<Score> BestScore = new List<Score>();
    public struct Score
    {
        public DateTime time;
        public float distance;

        public Score(DateTime time, float distance)
        {
            this.time = time;
            this.distance = distance;
        }
    }
    public void SaveGameSettings()
    {
        PlayerPrefs.SetInt("SavedVibration", vibration);
        PlayerPrefs.SetInt("SavedQuality", qualitySettings);
        PlayerPrefs.SetInt("SavedVolume", volume);
        PlayerPrefs.SetInt("StarsCount", starsCount);
        PlayerPrefs.SetInt("sensitivity", sensitivity);
        PlayerPrefs.SetInt("isFootballSold", ShopController.Current.isFootballSold);
        PlayerPrefs.SetInt("isBasketballSold", ShopController.Current.isBasketballSold);
        PlayerPrefs.SetInt("isKolobokSold", ShopController.Current.isKolobokSold);
    }

    public void SaveDistance()
    {
        var scoreCell = new Score(DateTime.Now, PlayerMovement.Current.distance);
        BestScore.Add(scoreCell);
        //File.Create();
    }

    public void SaveCurrentSkin()
    {
        PlayerPrefs.SetInt("currentSkin", ShopController.Current.currentSkin);
        SkinsManager.Current.ChangeSkin();
    }

    public void ChangeQuality()
    {
        if(qualitySettings == 1)
        {
            QualitySettings.SetQualityLevel(0);
            qualitySettings = 0;
        }
        else
        {
            QualitySettings.SetQualityLevel(1);
            qualitySettings = 1;
        }
        CanvasesManager.Current.SetQualityText();
    }

    public void VibrationChanger()
    {
        if (vibration == 1)
        {
           vibration = 0;
        }
        else
        {
            vibration = 1;
        }
        CanvasesManager.Current.SetVibrationText();
    }

    public void SaveStarsCount()
    {
        PlayerPrefs.SetInt("StarsCount", starsCount);    }

    private void Awake()
    {
        Current = this;
        LoadGameSettings();
    }

    private void LoadStarsCount()
    {
        if(PlayerPrefs.HasKey("StarsCount"))  starsCount = PlayerPrefs.GetInt("StarsCount");  
        CanvasesManager.Current.starsCount.text = "stars: " + starsCount.ToString() + "<sprite=0>";
    }

    private void LoadGameSettings()
    {
        CanvasesManager.Current.Canvases[5].SetActive(true);
        CanvasesManager.Current.Canvases[5].SetActive(false);
        LoadStarsCount();
        if (PlayerPrefs.HasKey("SavedVibration")) vibration = PlayerPrefs.GetInt("SavedVibration");
        if (PlayerPrefs.HasKey("SavedQuality")) qualitySettings = PlayerPrefs.GetInt("SavedQuality");
        if (PlayerPrefs.HasKey("sensitivity")) sensitivity = PlayerPrefs.GetInt("sensitivity");
        if (PlayerPrefs.HasKey("SavedVolume"))
        {
            volume = PlayerPrefs.GetInt("SavedVolume");
            if(CanvasesManager.Current != null)
            {
                CanvasesManager.Current.SetVibrationText();
                CanvasesManager.Current.SetQualityText();
            }
            if (AudioManager.Current != null)
            {
                AudioManager.Current.SetMasterVolume();
            }
        }

        if (PlayerPrefs.HasKey("isFootballSold")) ShopController.Current.isFootballSold = PlayerPrefs.GetInt("isFootballSold");
        if (PlayerPrefs.HasKey("isBasketballSold")) ShopController.Current.isBasketballSold = PlayerPrefs.GetInt("isBasketballSold");
        if (PlayerPrefs.HasKey("isKolobokSold")) ShopController.Current.isKolobokSold = PlayerPrefs.GetInt("isKolobokSold");
        if (PlayerPrefs.HasKey("currentSkin")) ShopController.Current.currentSkin = PlayerPrefs.GetInt("currentSkin");
    }
}
