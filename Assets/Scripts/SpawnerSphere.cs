﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Random = System.Random;

public class SpawnerSphere : MonoBehaviour
{
    public int guaranteedSpheres = 1;
    public List<GameObject> spawnpointsSpheres = new List<GameObject>();
    public List<GameObject> prefabsSphere = new List<GameObject>();
    public List<GameObject> spawnpointsColorWays = new List<GameObject>();
    public List<GameObject> spawnpointsBombs = new List<GameObject>();
    public List<GameObject> prefabsBombs = new List<GameObject>();
    public List<GameObject> spawnpointsPusher = new List<GameObject>();
    public List<GameObject> prefabsPusher = new List<GameObject>();
    public List<GameObject> spawnpointsFence = new List<GameObject>();
    public List<GameObject> prefabsFences = new List<GameObject>();
    public List<Transform> spheresCreated = new List<Transform>();
    public List<Transform> pushersCreated = new List<Transform>();
    public List<Transform> bombsCreated = new List<Transform>();
    public List<Transform> colorWaysCreated = new List<Transform>();
    public List<Transform> fencesCreated = new List<Transform>();

    private void Start()
    {
        Random random = new Random();
        int count = random.Next(spawnpointsSpheres.Count);
        if (count < guaranteedSpheres) count = guaranteedSpheres;
        for(int i = 0; i < count; i ++)
        {
            int spawnPointIdx = random.Next(spawnpointsSpheres.Count);
            int prefabIdx = random.Next(prefabsSphere.Count);
            if(spawnpointsSpheres[spawnPointIdx].transform.childCount == 0)
            {
                var sphere = Instantiate(prefabsSphere[prefabIdx], spawnpointsSpheres[spawnPointIdx].transform);
                spheresCreated.Add(sphere.transform);
            }
        }
        int colorWayIdx = random.Next(PrefabStorage.current.prefabsColorWays.Count); 
        int spawnPointColorWayIdx = random.Next(spawnpointsColorWays.Count);
        int bombSpawn = random.Next(0, 3);
        if(bombSpawn == 1)
        {
            int bombSpawnPointIdx = random.Next(spawnpointsBombs.Count);
            int prefabBombIdx = random.Next(prefabsBombs.Count);
            var bomb = Instantiate(prefabsBombs[prefabBombIdx], spawnpointsBombs[bombSpawnPointIdx].transform);
            bombsCreated.Add(bomb.transform);
        }
        else
        {
            int pusherSpawnChance = random.Next(0, 5);
            if (pusherSpawnChance == 1 && spawnpointsPusher.Count != 0)
            {
                int pusherPrefabIdx = random.Next(prefabsPusher.Count);
                int pusherSpawnPointIdx = random.Next(spawnpointsPusher.Count);
                var pusher = Instantiate(prefabsPusher[pusherPrefabIdx], spawnpointsPusher[pusherSpawnPointIdx].transform);
                pushersCreated.Add(pusher.transform);
            }
        }
        if (spawnpointsFence.Count != 0)
        {
            for (int i = 0; i < 2; i++)
            {
                var fenceIdx = random.Next(prefabsFences.Count);
                var fence = Instantiate(prefabsFences[fenceIdx], spawnpointsFence[i].transform);
                fencesCreated.Add(fence.transform);
            }
        }

        var colorway = Instantiate(PrefabStorage.current.prefabsColorWays[colorWayIdx], spawnpointsColorWays[spawnPointColorWayIdx].transform);
        colorWaysCreated.Add(colorway.transform);
    }
}
