﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ramp : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && PlayerMovement.Current.frwdSpeed <= PlayerMovement.Current.maxFrwdSpeed) PlayerMovement.Current.frwdSpeed += 5;
    }
}
