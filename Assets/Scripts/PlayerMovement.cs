﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using Random = System.Random;

public class PlayerMovement : MonoBehaviour
{
    public static PlayerMovement Current;
    public float distance;
    public CinemachineVirtualCamera cinemachineCamera;
    public string playerColor;
    public float playerRadius;
    public float acceleration;
    public float growEff;
    public float frwdSpeed;
    public float maxFrwdSpeed;
    public Rigidbody playerRb;
    public Transform _transform;
    public TrailRenderer trailRenderer;
    public Vector3 newSize;
    public TouchPad touchPad;
    public AudioSource audioSource;
    public bool isEnd = false;
    public List<ParticleSystem> playerParticleModule = new List<ParticleSystem>();
    public Vector3 targetPosition;
    public Vector3 targetEtalonPosition;
    public Vector3 targetAIPosition = Vector3.zero;
    [SerializeField] private float speed;
    [SerializeField] private float _widthOfSurface;
    [SerializeField] private List<Material> _materials = new List<Material>();
    [SerializeField] private List<Material> _materialsTrail = new List<Material>();
    private readonly List<string> _colors = new List<string>() {"red", "green", "yellow"};
    public MeshRenderer meshRenderer;
    public Color newcolor;
    public float trailSizeWidth;
    private Vector3 _prevPos;

    public void PlayerGrow()
    {
        ParticleManager.Current.ChangeColorEffect();
        if(playerRadius >= 3f)
        {
            WinEndGame();
        }
        else
        {
            playerRadius += growEff;
            trailSizeWidth = playerRadius; 
            newSize = new Vector3(playerRadius, playerRadius, playerRadius);
        }
    }
    public void PlayerDecrease()
    {
        if(playerRadius <= 0.25f)
        {
            DeathEndGame();
        }
        else
        {
            playerRadius -= growEff;
            trailSizeWidth = playerRadius;
            newSize = new Vector3(playerRadius, playerRadius, playerRadius);
        }
    }

    public void PlayerChangeColor(string color)
    {
        playerColor = color;
        var coloridx = _colors.IndexOf(color);
        newcolor = _materials[coloridx].color;
    }

    public void EndGame()
    {
        Settings.Current.SaveDistance();
        distance = 0;
        playerRb.isKinematic = true;
        trailRenderer.Clear();
        trailRenderer.enabled = false;
        newSize = Vector3.one;
        trailSizeWidth = 1f;
        trailRenderer.startWidth = trailSizeWidth;
        Settings.Current.SaveStarsCount();
        CanvasesManager.Current.starsCount.text = "Stars: " + Settings.Current.starsCount.ToString() + "<sprite=0>";
        CanvasesManager.Current.starsCount.gameObject.SetActive(true);
    }
    public void DeathEndGame()
    {
        Settings.Current.starsCount += (int)(20 * distance / 100);
        AudioManager.Current.PlayLooseSound();
        CanvasesManager.Current.Canvases[0].SetActive(true);
        EndGame();
    }

    public void WinEndGame()
    {
        Settings.Current.starsCount += (int)(50 * distance / 100);
        AudioManager.Current.PlayWinSound();
        CanvasesManager.Current.Canvases[2].SetActive(true);
        EndGame();
    }

    private void Awake()
    {
        Current = this;
        _transform = gameObject.GetComponent<Transform>();
        newSize = _transform.localScale;
        audioSource = GetComponent<AudioSource>();
    }
    private void Start()
    {
        playerRb = gameObject.GetComponent<Rigidbody>();
        if (_widthOfSurface == 0) _widthOfSurface = 1;
        playerRadius = transform.localScale.x;
        Random random = new Random();
        var coloridx = random.Next(_colors.Count);
        playerColor = _colors[coloridx];
        meshRenderer = gameObject.GetComponent<MeshRenderer>();
        trailRenderer = gameObject.GetComponent<TrailRenderer>();
        meshRenderer.material = _materials[coloridx];
        trailRenderer.material = _materialsTrail[coloridx];
        newcolor = meshRenderer.material.color;
        trailSizeWidth = 1f;
    }
    /// <summary>
    /// mouse controller
    /// </summary>
    ///
    // private void FixedUpdate()
    // {
    //     var mouseWorldPos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y,
    //         Camera.main.nearClipPlane));
    //     var targetPosition = _transform.position + new Vector3(0,0,1) * Time.fixedDeltaTime * frwdSpeed;
    //     targetPosition.x += mouseWorldPos.x / _widthOfSurface;
    //     float rotate = (float)(frwdSpeed / (2 * 3.14 * playerRadius) * 360);
    //     var additionalRotation = Quaternion.AngleAxis( rotate * Time.fixedDeltaTime, Vector3.right);
    //     _transform.rotation *= additionalRotation;
    //     playerRb.MovePosition(targetPosition);
    //     frwdSpeed += acceleration;
    //     if(_transform.localScale != newSize)
    //         _transform.localScale = Vector3.Lerp(_transform.localScale, newSize, 1f*Time.fixedDeltaTime);
    // }
    private void FixedUpdate()
    {
        if(playerRadius >= 3f)
        {
            audioSource.Stop();
            if(!isEnd)
            {
                isEnd = true;
                WinEndGame();
            }
        }
        if(playerRadius <= 0.25f)
        {
            audioSource.Stop();
            if(!isEnd)
            {
                isEnd = true;
                DeathEndGame();
            }
            isEnd = true;
        }
        audioSource.pitch = Vector3.Lerp(new Vector3(audioSource.pitch, 0,0),
            new Vector3(2 * frwdSpeed / maxFrwdSpeed,0,0), 1f * Time.deltaTime).x;
        if(frwdSpeed < maxFrwdSpeed) frwdSpeed += maxFrwdSpeed / frwdSpeed * acceleration;
        if(!playerRb.isKinematic)
        {
            if(_transform.position.y <= -10) DeathEndGame();
            targetPosition = _transform.position + new Vector3(0,0,1) * (Time.fixedDeltaTime * frwdSpeed);
            if (touchPad.directionDrag != Vector2.zero)
            {
                targetEtalonPosition.x = touchPad.startDragPlayerPosition.x + (-1 * touchPad.directionDrag.x * touchPad.distance * 4.5f);
            }
            if (touchPad.distance != 0)
                //targetPosition.x += -(touchPad.directionDrag.x * touchPad.distance * _widthOfSurface / Settings.Current.sensitivity) / 10;
                targetPosition.x = Vector3.Lerp(_transform.position, targetEtalonPosition, 0.2f).x; 
            float rotate = (float)(frwdSpeed / (2 * 3.14 * playerRadius) * 360);
            var additionalRotation = Quaternion.AngleAxis( rotate * Time.fixedDeltaTime, Vector3.right);
            _transform.rotation *= additionalRotation;
            if (targetPosition.x > 2.20) targetPosition.x = 2.20f;
            else if (targetPosition.x < -2.20) targetPosition.x = -2.20f;
            // if (targetAIPosition != Vector3.zero)
            // {
            //     targetPosition.x = targetAIPosition.x;
            //     targetAIPosition = Vector3.zero;
            // }
            playerRb.MovePosition(targetPosition);
            playerRadius -= 0.02f * Time.fixedDeltaTime;
            distance += frwdSpeed * Time.fixedDeltaTime;
            CanvasesManager.Current.distance.text = "Distance: " + ((int)distance).ToString();
        }
        newSize = new Vector3(playerRadius, playerRadius, playerRadius);
        trailSizeWidth = playerRadius;
        if(_transform.localScale != newSize)
                _transform.localScale = Vector3.Lerp(_transform.localScale, newSize, 1f*Time.fixedDeltaTime);
        if (meshRenderer.material.color != newcolor)
            meshRenderer.material.color =
                Vector4.Lerp(meshRenderer.material.color, newcolor, 2f * Time.fixedDeltaTime);
        if (trailRenderer.material.color != newcolor)
            trailRenderer.material.color =
                Vector4.Lerp(trailRenderer.material.color, newcolor, 2f * Time.fixedDeltaTime);
        if (Math.Abs(trailRenderer.startWidth - trailSizeWidth) > 0)
        {
            trailRenderer.startWidth = Vector2.Lerp(new Vector2(trailRenderer.startWidth, 0),
                new Vector2(trailSizeWidth, 0), 1f * Time.deltaTime).x;
        }
    }
}
