﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fence : MonoBehaviour
{
    public string colorOfFence;
    private void OnTriggerEnter(Collider other)
    { 
        if (other.CompareTag("Player") && PlayerMovement.Current.playerColor != colorOfFence)
        {
            PlayerMovement.Current.playerRadius -= 0.5f;
        }
    }
}
