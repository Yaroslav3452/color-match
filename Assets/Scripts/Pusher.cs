﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

public class Pusher : MonoBehaviour
{
    public LayerMask targetLayer;
    
    public void Kaboom(Transform transform, int radius)
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius, targetLayer);
        foreach (var hit in hitColliders)
        {
            var rb = hit.GetComponent<Rigidbody>();
            rb.isKinematic = false;
            rb.AddExplosionForce(20f, transform.position, 32f, 0.5f, ForceMode.VelocityChange);
        }
    }
    private IEnumerator OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if(Settings.Current.vibration == 1) Handheld.Vibrate();
            PlayerMovement.Current.frwdSpeed -= PlayerMovement.Current.frwdSpeed * 0.1f;
            var position = transform.position;
            AudioManager.Current.PlayPusherSound(position);
            var rb = GetComponent<Rigidbody>();
            rb.isKinematic = false;
            rb.AddForce(new Vector3(0,0,1) * rb.mass * 20, ForceMode.Impulse);
            yield return new WaitForSecondsRealtime(2);
            Kaboom(transform, 5);
            ParticleManager.Current.BoomEffect(transform);
            AudioManager.Current.PlayBombSound(position);
            rb.AddExplosionForce(20f, position, 32f, 5f, ForceMode.VelocityChange);
            Destroy(gameObject);
        }
        else if (other.CompareTag("SphereOfPush"))
        {
            var position = transform.position;
            AudioManager.Current.PlayPusherSound(position);
            var rb = GetComponent<Rigidbody>();
            rb.isKinematic = false;
            rb.AddForce(new Vector3(0,0,1) * rb.mass * 20, ForceMode.Impulse);
            yield return new WaitForSecondsRealtime(2);
            Kaboom(transform, 5);
            ParticleManager.Current.BoomEffect(transform);
            AudioManager.Current.PlayBombSound(position);
            rb.AddExplosionForce(20f, position, 32f, 5f, ForceMode.VelocityChange);
            Destroy(gameObject);
        }
    }
}
